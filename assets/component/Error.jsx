import React from 'react'

const Error = () => {
    return (
        <>
<section className="content">
  <div className="error-page">
    <h2 className="headline text-warning"> 404</h2>
    <div className="error-content">
      <h3><i className="fas fa-exclamation-triangle text-warning" /> Oops! Page not found.</h3>
    </div>

  </div>

</section>

  
        </>
    )
}

export default Error
